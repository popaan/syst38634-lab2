package time;

import static org.junit.Assert.*;

/*
 * Author: Andreea Popa
 * */

import org.junit.Test;

public class TimeTest {
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("23:59:59");		
		assertTrue("The time provided does not match the result", totalSeconds == 86399);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");		
		fail("The time provided is not valid");
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0H");		
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalMillisecondsRegualr() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsExceptional() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	

}
